const ERROR = 'error';
// const WARNING = 'warn';
const OFF = 'off';

module.exports = {
  'extends': 'eslint:recommended',
  'parser': '@typescript-eslint/parser',
  'plugins': [
    'import',
    'jest'
  ],
  'rules': {
    'no-unused-vars': [ERROR, {'vars': 'local', 'args': 'none'}],
    'quotes': [ERROR, 'single', {'allowTemplateLiterals': true}],
    'semi': ERROR,
    'indent': [ERROR, 2],
    'no-multi-spaces': ERROR,
    // 'eqeqeq': ERROR,

    //ES6
    'constructor-super': ERROR,
    'arrow-spacing': ERROR,
    'no-const-assign': ERROR,
    'no-var': ERROR,
    'prefer-const': ERROR,
    'prefer-spread': ERROR,
    'prefer-template': ERROR,
    'no-dupe-class-members': ERROR,
    'no-this-before-super': ERROR,
    'require-yield': OFF,
    'no-useless-escape': OFF,

    //Modules
    'import/no-commonjs': ERROR,
    'import/first': ERROR,
    'import/no-duplicates': ERROR,
    'import/extensions': ERROR,
    'import/newline-after-import': ERROR,
    'import/named': ERROR,
  },
  'env': {
    'es6': true,
    'commonjs': true,
    'jest': true,
    'node': true
  },
  'globals': {
    'console': true,
    'expect': true
  }
};
