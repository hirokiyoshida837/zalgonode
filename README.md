# zalgonode

[![pipeline status](https://gitlab.com/hirokiyoshida837/zalgonode/badges/master/pipeline.svg)](https://gitlab.com/hirokiyoshida837/zalgonode/-/commits/master)
[![coverage report](https://gitlab.com/hirokiyoshida837/zalgonode/badges/master/coverage.svg)](https://gitlab.com/hirokiyoshida837/zalgonode/-/commits/master)
---

## about

`z̘̯̙̈̑ã̺̇̚̚ḽ̷̱̦̽g̰̉̄̽̏ȫ̢̞̙n̖̬̊̒̽o̬̘̠̫̒d̡̨̟̺̫ḙ̬̠̅̒` is a text glitch tool.


## Getting Started

### installation

install from npm
- [@h.yoshida/zalgonode - npm](https://www.npmjs.com/package/@h.yoshida/zalgonode)

```sh
$ npm i @h.yoshida/zalgonode
```

### usage

use glitcher like below code.

```typescript
import {Glitcher} from "glitcher";

const originalString = 'this is sample string'

const glitched = new Glitcher().glitch('this is sample string');

// output sample t̷̸̏̉̚ȟ̵̠̟̲i̫̜̘̙̊s̓̽̃̍̚ ̢̭̳̈̃ĭ̮̼̥̙s̜̥̪̺̓ ̸̭̎̊̇s̪̱̉̚̚a̶̛̮̮̭m̠̜̘̑̈p̤̮̫̪̔l̻̗̓̊̎ę̺̤̑̇ ̶̝̬̰̙ŝ̯̘̑̇t̶̳̓̋̇r̷̛̠̤̐i̴̶̟̺̍ṅ̡̯̮̜ǧ̻̪̗̞
console.log(glitched);
```

#### glitch amount

You can specify the amount of glitching scale. default is `5`.
```typescript
// t̎h̔i̍s̟ ̅i̭s̆ ̈s̸a̪ṃp̕ľe̝ ̂s̵t̨r̠i̹n̗g̏
console.log(new Glitcher(1).glitch('this is sample string', 1));

// ţ̜̥̹̖̺̤̘̂̊h̵̡̳̦̝̎̊̊̏̃i̢̨̧̬̮̣̟̋̐̐s̸̛̭̭̍̌̏̄̅̕ ̸̣̭̹̹̻̒̏̊̓i̬̱̫̣̼̽̽̂̅̐ş̵̛̫̞̥̔̔̄̒ ̧̘̟̻̤̤̮̼̒̒s̩̮̬̋̊̉̎̉̐̌ą̶̵̢̘́̎̽̇̓m̴̞̖̖̜̫̘̩̒̒p̴̢̱̼̱̪̻̋̽̚l̴̠̰̦̘̙̘̞̂̽e̼̥̝̮̲̪̻̽̂̍ ̦̗̦̌̅̄̈́̔̕s̷̯̟̳̲̞̣̈̉̂ť̵̻̪̠̫̣̈̉̕r̢̹̯̼̳̻̔̊̊̕ĩ̷̢̢̨̧̮̒̑̑n̡̜̣̐̽̃̈̈̎̑g̬̬̫̔̄̌̑́̋́
console.log(new Glitcher(10).glitch('this is sample string', 1));
```

#### glitch type

You can choose the type of glitch effect, `top`, `bottom`, and `middle` .

```typescript
const glitcher = new Glitcher(5)
    .doTopSideGlitch(true)
    .doBottomSideGlitch(false)
    .doMiddleSideGlitch(false);

//  z̈̉̓̃̀a̓̃̎̇̕ĺ̇̀̆̐ġ̅̍̆̍ö́̒̄̚n̐̈̋̀̌ȏ̃̔̔̄d̓̀̎̍̏ḗ̋̍̕
glitcher.glitch('zalgonode')
```

## restriction

`zalgonode` doesn't support Non-alphabetic characters and surrogate pair.
(not guaranteed to succeed, when glitching non-alphabetic chars.)


## reference

- [Diacritic - Wikipedia](https://en.wikipedia.org/wiki/Diacritic)
- [Combining character - Wikipedia](https://en.wikipedia.org/wiki/Combining_character#Zalgo_text)
- [Zalgo Text Generator by Tchouky](http://www.eeemo.net/)
- [ダイアクリティカルマーク - Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%80%E3%82%A4%E3%82%A2%E3%82%AF%E3%83%AA%E3%83%86%E3%82%A3%E3%82%AB%E3%83%AB%E3%83%9E%E3%83%BC%E3%82%AF)
- [合成可能なダイアクリティカルマーク - Wikipedia](https://ja.wikipedia.org/wiki/%E5%90%88%E6%88%90%E5%8F%AF%E8%83%BD%E3%81%AA%E3%83%80%E3%82%A4%E3%82%A2%E3%82%AF%E3%83%AA%E3%83%86%E3%82%A3%E3%82%AB%E3%83%AB%E3%83%9E%E3%83%BC%E3%82%AF)

## License

MIT
