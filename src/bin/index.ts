#!/usr/bin/env node

import {Glitcher} from '../lib/glitch/Glitcher';

// とりあえず引数を一つだけ
console.log(new Glitcher().glitch(process.argv[2]));
