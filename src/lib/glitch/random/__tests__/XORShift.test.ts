import {XORShift32} from '../XORShift32';

test('xor_shift_01', () => {
  const random = new XORShift32().getNext();
  expect(random).toBe(-593279510);
});

test('xor_shift_02', () => {
  const random = new XORShift32(88675123).getNext();
  expect(random).toBe(-593279510);
});

test('xor_shift_03', () => {
  const generator = new XORShift32(88675123);
  const first = generator.getNext();
  expect(first === generator.getNext()).toBe(false);
});

test('xor_shift_nextInt_01', () => {
  const random = new XORShift32().getNextInt(10, 20);
  expect(random).toBe(20);
});

test('xor_shift_nextInt_02', () => {
  const random = new XORShift32(88675123).getNextInt(10, 20);
  expect(random).toBe(20);
});

test('xor_shift_nextInt_03', () => {
  const generator = new XORShift32(88675123);
  const first = generator.getNextInt(10, 20);
  expect(first === generator.getNextInt(10, 20)).toBe(false);
});
