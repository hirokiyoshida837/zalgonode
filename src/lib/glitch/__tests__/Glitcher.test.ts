import {Glitcher} from '../Glitcher';

const glitchBaseText = 'the quick brown fox jumps over the lazy dog';
// const glitchBaseText = 'zalgonode';

test('01_glitch', () => {

  const glitcher = new Glitcher(3);

  const s = glitcher.glitch(glitchBaseText);

  console.log(s);
});

// シード値を指定して生成すると、毎回固定の文字列で生成される。
test('01_glitch_seed', () => {

  const glitcher = new Glitcher(3, 1);

  const s = glitcher.glitch(glitchBaseText);

  console.log(s);
  expect(s).toBe('ț̶̇ȟ̤̚ẽ̙̖ ̧̯̕q̻̖̝u̢̳̍ȋ̷̹ç̉̈k̟̳̋ ̢̙̥b̴̲̩r̜̃̔ó̟̮w̝̜̹n̗̜̄ ̍̋̊f̦̯̓ȍ̧̼x̡̨̆ ̡́̇j̡̰̃u̩̖̍m̰̔̄p̈̓̚s̢̘̎ ̥̯̃ơ̵̡v̇̒̃ệ̋r̴̲̆ ̡̧̪t̡̥̃h̵̴̰ĕ̷̃ ̘̊́l̗̫̄â̸̑z̸̐́y̸̸̥ ̶̶̪d̩̫̄ỏ̩̠g̭̩̊');
});


test('02_glitch', () => {

  const glitcher = new Glitcher(5);

  const s = glitcher.glitch(glitchBaseText);
  console.log(s);

  const x = glitcher.glitch(glitchBaseText, 2);
  console.log(x);
});

test('03_glitch_only_top', () => {

  const glitcher = new Glitcher(5)
    .doTopSideGlitch(true)
    .doBottomSideGlitch(false)
    .doMiddleSideGlitch(false);

  const s = glitcher.glitch(glitchBaseText);

  console.log(s);
});

test('04_glitch_only_bottom', () => {

  const glitcher = new Glitcher(5)
    .doTopSideGlitch(false)
    .doBottomSideGlitch(true)
    .doMiddleSideGlitch(false);

  const s = glitcher.glitch(glitchBaseText);

  console.log(s);
});

test('05_glitch_only_mid', () => {

  const glitcher = new Glitcher(5)
    .doTopSideGlitch(false)
    .doBottomSideGlitch(false)
    .doMiddleSideGlitch(true);

  const s = glitcher.glitch(glitchBaseText);

  console.log(s);
});

test('Glitch_Type_01', () => {

  const glitcher = new Glitcher(2, 1)
    .setGlitchType({top: false, btm: true, mid: false});

  const s = glitcher.glitch(glitchBaseText);

  expect(s).toBe('ţ̬ḩ̥e̡̯ ̠̳q̯̱u̦̤i̱̙c̥̺k̳̻ ̗̹b̺̳r̫̰ǫ̦w̟̬ṋ̡ ̺̯f̡̬o̡̼x̖̬ ̘̗j̘̲u̬̟m̡̼p̻̼s̱̞ ̤̲o̭̙v̟̟ḙ̱ṛ̤ ̹̯t̯̝h̯̹e̯̲ ̟̘l̲̻a̩̭z̰̠y̫̤ ̗̲d̠̳o̧̧g̨̙');
});

test('Glitch_Type_02', () => {

  const glitcher = new Glitcher(2, 10)
    .setGlitchType({top: false});

  const s = glitcher.glitch(glitchBaseText);

  console.log(JSON.stringify(glitcher));

  expect(s).toBe('ṭ̭h̥̱e̘̯ ̗̹q̸̼u̡̠ị̟c̖̪k̴̞ ̴̥b̴̖r̢̨o̳̯w̶̫n̖̺ ̨̳f̶̹o̵̬x̷̟ ̷̡j̢̞u̸̹m̹̳p̢̣s̸̵ ̦̜o̹̳v̡̠e̷̴r̩̼ ̻̝t̻̪h̼̩e̡̤ ̨̩ḻ̻a̻̦z̷̥y̶̗ ̢̲ḏ̙o̯̹g̙̮');
});

