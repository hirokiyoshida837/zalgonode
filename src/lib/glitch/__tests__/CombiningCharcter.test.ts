test('list up combining characters', () => {
  const array = new Array<string>();

  for (let i = 768; i < 829; i++) {
    array.push(`0x${i.toString(16).padStart(4, '0')}, ${i}, a${String.fromCodePoint(i)} r`);
  }

  console.info(array);
});

test('list up combining characters_ext', () => {
  const array = new Array<string>();

  for (let i = 829; i < 829 + 24; i++) {
    array.push(`0x${i.toString(16).padStart(4, '0')}, ${i}, a${String.fromCodePoint(i)} r`);
  }

  console.info(array);
});
