/** @type {import('@jest/types/build/Config').InitialOptions} */
module.exports = {
  preset: 'ts-jest',
  testMatch: ['<rootDir>/**/*.test.ts'],
  collectCoverage: false,
  errorOnDeprecated: true,
  testEnvironment: 'node',
};
